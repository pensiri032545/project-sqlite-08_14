class ProfileModel {
  int? id;
  String firstname;
  String lastname;
  String email;
  String image;

  ProfileModel({
    this.id,
    required this.firstname,
    required this.lastname,
    required this.email,
    required this.image,

  });

  factory ProfileModel.fromMap(Map<String, dynamic> json) =>
      new ProfileModel(
        id: json['id'],
        firstname: json['firstname'],
        lastname: json['lastname'],
        email: json['email'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
      'image': image,
    };
  }
}
