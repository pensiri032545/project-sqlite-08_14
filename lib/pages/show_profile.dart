import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_note/database/database_helper.dart';
import 'package:project_note/model/profile_model.dart';


class ShowProfile extends StatelessWidget {
  final id;
  ShowProfile({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Profile'),
      ),
      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                children: snapshot.data!.map((profile) {
                  return Center(
                    child: Column(
                      children: [
                        SizedBox(height: 20,),
                        CircleAvatar(
                          backgroundImage: FileImage(File(profile.image)),
                          radius: 100,
                        ),
                        SizedBox(height: 20,),
                        Text('Menu: ${profile.firstname}',style: TextStyle(fontSize: 30),),
                        Text('Price: ${profile.lastname}',style: TextStyle(fontSize: 30),),
                        Text('Details: ${profile.email}',style: TextStyle(fontSize: 30),),
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}

