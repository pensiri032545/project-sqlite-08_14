import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
        backgroundColor: Colors.brown.shade400,
      ),
      body: Container(
          constraints: BoxConstraints.expand(),
          child: SingleChildScrollView(
            child: Column(
              children: const [
                CircleAvatar(
                    radius: 100,
                    backgroundImage: AssetImage('assets/images/imgP1.jpg',)
                ),
                Text('6450110008 น.ส.เพ็ญศิริ อินทร์ประหยัด IDTM',style: TextStyle(fontSize: 20),),
                SizedBox(height: 50,),
                CircleAvatar(
                  radius: 100,
                  backgroundImage: AssetImage('assets/images/imgP2.jpg'),
                ),
                Text('6450110014 น.ส.สุนิตา เรืองนุ้ย IDTM',style: TextStyle(fontSize: 20),),
                SizedBox(height: 50,),
                Text('เหนื่อยกับงานว่าแย่แล้ว เหนื่อยกับคนเห็นแก่ตัวนั้นแย่กว่า',style: TextStyle(fontSize: 16),),

              ],
            ),
          )
      ),
    );
  }
}

