import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:developer' as developer;

import 'package:image_picker/image_picker.dart';
import 'package:project_note/database/database_helper.dart';
import 'package:project_note/model/profile_model.dart';

class AddProfile extends StatefulWidget {
  AddProfile();

  @override
  _AddProfileState createState() => _AddProfileState();
}

class _AddProfileState extends State<AddProfile> {
  var firstname = TextEditingController();
  var lastname = TextEditingController();
  var email = TextEditingController();
  var phone = TextEditingController();
  var _image;
  var imagePicker;


  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Manu'),
      ),
      body: Container(
        color: Colors.grey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                            source: ImageSource.gallery,
                            imageQuality: 50,
                            //preferredCameraDevice: CameraDevice.front
                          );
                          setState(() {
                            _image = File(image.path);
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                          BoxDecoration(color: Colors.blueAccent[200]),
                          child: _image != null
                              ? Image.file(
                            _image,
                            width: 200.0,
                            height: 200.0,
                            fit: BoxFit.fitHeight,
                          )
                              : Container(
                            decoration: BoxDecoration(
                                color: Colors.white),
                            width: 200,
                            height: 200,
                            child: Icon(
                              Icons.camera_alt,
                              color: Colors.grey[800],
                            ),
                          ),
                        ),
                      ),
                    ),
                    buildTextfield('Enter Menu', firstname),
                    buildTextfield('Enter Price', lastname),
                    buildTextfield('Enter Details', email),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Add'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {

        if (title == 'Add') {
          await DatabaseHelper.instance.add(
            ProfileModel(
                firstname: firstname.text,
                lastname: lastname.text,
                email: email.text,
                image: _image.path),
          );
          setState(() {
            //firstname.clear();
            //selectedId = null;
          });

          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          fixedSize: Size(120, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

