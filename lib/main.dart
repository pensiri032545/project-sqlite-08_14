import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:project_note/pages/list_profile.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite Project',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: const ListProfile(title: 'Menu Lists'),
      debugShowCheckedModeBanner: false,
    );

  }
}


